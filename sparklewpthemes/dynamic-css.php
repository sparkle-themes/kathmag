<?php
/**
 * Dynamic css
*/
if ( ! function_exists( 'kathmag_dynamic_color_css' ) ) {

    function kathmag_dynamic_color_css() {
        
        $primary_color = get_theme_mod('kathmag_primary_color', '#000000');
        $secondary_color = get_theme_mod('kathmag_secondary_color', '#CC1919');  
    ?>               
        <style type="text/css" media="screen">
            /**
             * Primary Color 
            */
            .primary_navigation.dark,
            .primary_navigation.dark ul ul,
            .search_modal .modal-content .search_form button, .widget .search-form button,
            #toTop:hover,
            .pagination .page-numbers.current,
            #comments form input[type="submit"]:hover,
            .wpcf7 input[type="submit"], .wpcf7 input[type="button"],
            .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button,
            .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,
            .woocommerce #respond input#submit.alt.disabled, .woocommerce #respond input#submit.alt.disabled:hover, .woocommerce #respond input#submit.alt:disabled, .woocommerce #respond input#submit.alt:disabled:hover, .woocommerce #respond input#submit.alt:disabled[disabled], .woocommerce #respond input#submit.alt:disabled[disabled]:hover, .woocommerce a.button.alt.disabled, .woocommerce a.button.alt.disabled:hover, .woocommerce a.button.alt:disabled, .woocommerce a.button.alt:disabled:hover, .woocommerce a.button.alt:disabled[disabled], .woocommerce a.button.alt:disabled[disabled]:hover, .woocommerce button.button.alt.disabled, .woocommerce button.button.alt.disabled:hover, .woocommerce button.button.alt:disabled, .woocommerce button.button.alt:disabled:hover, .woocommerce button.button.alt:disabled[disabled], .woocommerce button.button.alt:disabled[disabled]:hover, .woocommerce input.button.alt.disabled, .woocommerce input.button.alt.disabled:hover, .woocommerce input.button.alt:disabled, .woocommerce input.button.alt:disabled:hover, .woocommerce input.button.alt:disabled[disabled], .woocommerce input.button.alt:disabled[disabled]:hover,
            .woocommerce-account .woocommerce-MyAccount-navigation ul li a{
                background-color: <?php echo esc_attr( $primary_color ); ?>;
            }

            .woocommerce div.product .woocommerce-tabs ul.tabs li:hover, .woocommerce div.product .woocommerce-tabs ul.tabs li.active{
                background-color: <?php echo esc_attr( $primary_color ); ?> !important;
            }

            .km_general_header .top_header .top_left ul a,
            .km_general_header .top_header .top_right ul a,
            .woocommerce-message:before,
            .woocommerce-info:before,
            .woocommerce-error:before,
            .woocommerce-account .woocommerce-MyAccount-navigation ul li.is-active a, .woocommerce-account .woocommerce-MyAccount-navigation ul li:hover a{
                color: <?php echo esc_attr( $primary_color ); ?>;
            }

            .pagination .page-numbers,
            #comments form input[type="submit"]:hover,
            .wpcf7 input[type="submit"], .wpcf7 input[type="button"],
            .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button,
            .woocommerce div.product .woocommerce-tabs ul.tabs:before,
            .cart_totals h2, .cross-sells>h2, .woocommerce-billing-fields h3, .woocommerce-additional-fields h3, .related>h2, .upsells>h2, .woocommerce-shipping-fields>h3, .woocommerce-Address-title h3,
            .woocommerce-message,
            .woocommerce-cart .wc-proceed-to-checkout a.checkout-button,
            .woocommerce-info,
            .woocommerce-error,
            .woocommerce-account .woocommerce-MyAccount-content,
            .woocommerce-account .woocommerce-MyAccount-navigation ul li a,
            .woocommerce-account .woocommerce-MyAccount-navigation ul li.is-active a, .woocommerce-account .woocommerce-MyAccount-navigation ul li:hover a{
                border-color: <?php echo esc_attr( $primary_color ); ?>;
            }
            
            /**
             * Secondary Color 
            */
            .main_navigation ul li a:hover, .main_navigation ul li.current-menu-item.current_page_item a,
            .ticker_wrap span.ticker_haeading,
            #toTop,
            .main_navigation .primary_navigation ul li.primarynav_search_icon,
            .widget_tag_cloud .tagcloud a:hover,
            #comments form input[type="submit"],
            .wpcf7 input[type="submit"]:hover, .wpcf7 input[type="button"]:hover,
            .woocommerce a.added_to_cart,
            .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover,
            .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover{
                background-color: <?php echo esc_attr( $secondary_color ); ?>;
            }

            .ticker_wrap span.ticker_haeading::after{
                border-color: transparent transparent transparent <?php echo esc_attr( $secondary_color ); ?>;
            }

            .section_title h2, .section_title h3, .section_title h4, .widget_title h2, .widget_title h3, .widget_title h4,
            .widget_tag_cloud .tagcloud a:hover,
            .single .the_content blockquote,
            #comments h2.comments-title:after,
            #comments div#respond h3#reply-title::after,
            #comments form input[type="submit"],
            .wpcf7 input[type="submit"]:hover, .wpcf7 input[type="button"]:hover,
            .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover{
                border-color: <?php echo esc_attr( $secondary_color ); ?>;
            }

            .km_general_header .top_header .top_left ul a:hover,
            .km_general_header .top_header .top_right ul a:hover,
            .km_banner.km_banner_layout_three .post_fimage .post_meta .post_title h2 a:hover,
            .km_banner.km_banner_layout_three .post_fimage .post_meta .posted_date a:hover,
            .km_banner .km_g_banner .item .col .post_meta .post_title h2 a:hover,
            .km_banner .km_g_banner .item .col .post_meta .posted_date a:hover,
            .km_featured_posts .fp_carousel .fp_card .post_meta .post_title h3 a:hover,
            .km_featured_posts .fp_carousel .fp_card .post_meta .posted_date a:hover,
            .post_meta ul li span a:hover,
            .post_meta ul li a:hover,
            a:hover,
            .km_posts_widget_layout_eight .km_card_holder_wig_eight .post_card .post_fimage .post_details .post_title h3 a:hover,
            .km_posts_widget_layout_eight .km_card_holder_wig_eight .post_card .post_fimage .post_details .post_meta ul li span a:hover,
            .widget a:hover, .widget_archive a:hover, .widget_categories a:hover, .widget_recent_entries a:hover, .widget_meta a:hover, .widget_product_categories a:hover, .widget_rss li a:hover, .widget_pages li a:hover, .widget_nav_menu li a:hover,
            .related_posts_carousel .fp_card .post_meta .post_title h3 a:hover,
            .related_posts_carousel .fp_card .post_meta .posted_date a:hover{
                color: <?php echo esc_attr( $secondary_color ); ?>;
            }
        </style>

    <?php }

}
add_action( 'wp_head', 'kathmag_dynamic_color_css' );
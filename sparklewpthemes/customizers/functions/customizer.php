<?php
/**
 * Kathmag Theme Customizer
 *
 * @package Kathmag
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function kathmag_customize_register( $wp_customize ) {

	// Dropdown Category Class
	require get_theme_file_path( 'sparklewpthemes/customizers/functions/controls.php' );

	// Sanitization Callback
	require get_theme_file_path( 'sparklewpthemes/customizers/functions/sanitize.php' );

	// Customization Options
	require get_theme_file_path( 'sparklewpthemes/customizers/options/option-init.php' );

	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->get_setting( 'custom_logo' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {

		$wp_customize->selective_refresh->add_partial( 'custom_logo', array(
			'selector'        => '.custom-logo-link',
			'render_callback' => '_return_false',
		) );

		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'kathmag_customize_partial_blogname',
		) );

		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'kathmag_customize_partial_blogdescription',
		) );
	}


	/**
	 * Add General Settings Panel
	 *
	 * @since 1.0.0
	*/
	$wp_customize->add_panel(
	    'kathmag_general_settings_panel',
	    array(
	        'priority'       => 3,
	        'title'          => esc_html__( 'General Settings', 'kathmag' ),
	    )
	);


	$wp_customize->get_section( 'title_tagline' )->panel = 'kathmag_general_settings_panel';
	$wp_customize->get_section( 'title_tagline' )->priority = 5;

	$wp_customize->get_section( 'header_image' )->panel = 'kathmag_general_settings_panel';
	$wp_customize->get_section( 'header_image' )->priority = 7;

	//$wp_customize->get_section( 'colors' )->panel = 'kathmag_light_general_settings_panel';
	$wp_customize->get_section( 'colors' )->title = esc_html__('Theme Colors Settings', 'kathmag');
	$wp_customize->get_section( 'colors' )->priority = 8;

	$wp_customize->register_section_type('Kathmag_Upgrade_Section');

	// Primary Color.
	$wp_customize->add_setting('kathmag_primary_color', array(
	    'default' => '#000000',
	    'sanitize_callback' => 'sanitize_hex_color',
	));

	$wp_customize->add_control('kathmag_primary_color', array(
	    'type' => 'color',
	    'label' => esc_html__('Primary Color', 'kathmag'),
	    'section' => 'colors',
	));


	$wp_customize->add_setting('kathmag_secondary_color', array(
	    'default' => '#CC1919',
	    'sanitize_callback' => 'sanitize_hex_color',
	));

	$wp_customize->add_control('kathmag_secondary_color', array(
	    'type' => 'color',
	    'label' => esc_html__('Secondary Primary Color', 'kathmag'),
	    'section' => 'colors',
	));

	$wp_customize->add_setting('colors_upgrade_text', array(
        'sanitize_callback' => 'kathmag_sanitize_text'
    ));

    $wp_customize->add_control(new Kathmag_Upgrade_Text($wp_customize, 'colors_upgrade_text', array(
        'section' => 'colors',
        'label' => esc_html__('For more styling,', 'kathmag'),
        'choices' => array(
            esc_html__('Change Menu Font and Hover Color', 'kathmag'),
            esc_html__('Change Footer Top Background and Bottom Background Color', 'kathmag'),
            esc_html__('Change Footer Text Color', 'kathmag'),
            esc_html__('Change Footer Anchor and Hover Color', 'kathmag'),
            esc_html__('Option to Reset Colors', 'kathmag'),
        ),
        'priority' => 100
    )));


	$wp_customize->get_section( 'background_image' )->panel = 'kathmag_general_settings_panel';
	$wp_customize->get_section( 'background_image' )->priority = 15;

	$wp_customize->get_section( 'static_front_page' )->panel = 'kathmag_general_settings_panel';
	$wp_customize->get_section( 'static_front_page' )->priority = 20;

}
add_action( 'customize_register', 'kathmag_customize_register' );

/**
 * Load active callback functions
 */
require get_theme_file_path( 'sparklewpthemes/customizers/functions/active.php' );

/**
 * Load choices used in customizer options
 */
require get_theme_file_path( 'sparklewpthemes/customizers/functions/choices.php' );

/**
 * Load default values of customizer options
 */
require get_theme_file_path( 'sparklewpthemes/customizers/functions/defaults.php' );

/**
 * Load yynamic CSS
 */
require get_theme_file_path( 'sparklewpthemes/customizers/functions/dynamic.php' );

//SANITIZATION FUNCTIONS
function kathmag_sanitize_text($input) {
    return wp_kses_post(force_balance_tags($input));
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function kathmag_customize_partial_blogname() {

	bloginfo( 'name' );

}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function kathmag_customize_partial_blogdescription() {

	bloginfo( 'description' );

}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function kathmag_customize_preview_js() {

	wp_enqueue_script( 'kathmag-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20180319', true );

}
add_action( 'customize_preview_init', 'kathmag_customize_preview_js' );

/**
 * Enqueue Styles and Scripts for admin panel
 */
function kathmag_enqueue_admin_scripts() {

	wp_enqueue_style( 'chosen', get_template_directory_uri() . '/sparklewpthemes/customizers/assets/css/chosen.css' );	

	wp_enqueue_style( 'kathmag-custom-style', get_template_directory_uri() . '/sparklewpthemes/customizers/assets/css/custom-style.css' );

	wp_enqueue_script( 'chosen', get_template_directory_uri() . '/sparklewpthemes/customizers/assets/js/chosen.js', array('jquery'), '1.8.3', true );	

	wp_enqueue_script( 'kathmag-custom-script', get_template_directory_uri() . '/sparklewpthemes/customizers/assets/js/custom-script.js', array('jquery'), '20180319', true );	
}
add_action( 'customize_controls_enqueue_scripts', 'kathmag_enqueue_admin_scripts' );

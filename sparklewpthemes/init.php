<?php
/**
 * Main Custom admin functions area
 *
 * @since SparkleThemes
 *
 * @param Kathmag
 *
 */


/**
 * Custom functions that act independently of the theme header.
*/
require get_theme_file_path('sparklewpthemes/core/custom-header.php');

/**
 * Custom functions that act independently of the theme templates.
*/
require get_theme_file_path('sparklewpthemes/core/template-functions.php');

/**
 * Custom template tags for this theme.
*/
require get_theme_file_path('sparklewpthemes/core/template-tags.php');

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {

   require get_theme_file_path('sparklewpthemes/core/jetpack.php');

}

/**
* Load WooCommerce compatibility file.
*/
if ( class_exists( 'WooCommerce' ) ) {

	require get_theme_file_path('sparklewpthemes/woocommerce.php');

}


/**
 * Load Customizer Additions File.
*/
require get_theme_file_path('sparklewpthemes/customizers/functions/customizer.php');


/**
 * Load breadcrumb File
 */
require get_theme_file_path('sparklewpthemes/third-party/breadcrumbs.php');


/**
 * Load filters File
 */
require get_theme_file_path('sparklewpthemes/functions/filters.php');


/**
 * Load helpers File
 */
require get_theme_file_path('sparklewpthemes/functions/helpers.php');


/**
 * Load hooks File
 */
require get_theme_file_path('sparklewpthemes/functions/hooks.php');

/**
 * Load widgets File
 */
require get_theme_file_path('sparklewpthemes/theme-widgets/widget-init.php');


/**
 * Load Meta Boxes
 */
require get_theme_file_path('/sparklewpthemes/meta-boxes/post-metabox.php');
require get_theme_file_path('/sparklewpthemes/meta-boxes/page-metabox.php');


/**
 * Welcome Page.
 */
if ( is_admin() ) {

	require get_template_directory() . '/welcome/welcome.php';
	
}

/**
 * Demo Import.
*/
//require get_template_directory() . '/welcome/importer.php';

/**
 * Load in customizer upgrade to pro
*/
require get_theme_file_path('sparklewpthemes/customizers/customizer-pro/class-customize.php');

/**
 * Dunamic CSS Color Options file.
*/
require get_theme_file_path() .'/sparklewpthemes/dynamic-css.php';
=== Kathmag ===
Contributors: sparklewpthemes
Tags: custom-background, custom-logo, custom-menu, featured-images, threaded-comments, translation-ready, one-column, two-columns, three-columns, left-sidebar, right-sidebar, blog, news, post-formats, theme-options, footer-widgets, e-commerce
Requires at least: 4.7
Tested up to: 5.8
Requires PHP: 5.2.4
Stable tag: 1.1.1
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Kathmag is a simple and clean, ultra-fast free WordPress Magazine theme, Kathmag is specially designed for magazine, News portal, Blog and digital content publishing websites. Kathmag completely bases on customizer, where allows you to customize theme settings & layout with live preview, especially three different block/section help you to display post different style and easily ordering post block section, also include 4+ custom widgets and compatible with multiple different plugins.

== Description ==

Kathmag is a simple and clean, ultra-fast free WordPress Magazine theme, Kathmag is specially designed for magazine, News portal, Blog and digital content publishing websites. It is a modern theme with elegant and responsive design and is completely built on customizer, where allows you to customize theme settings & layout with live preview, especially in front page three different block/section which helps you to display post different style with different layout and easily re-ordering each and every post block section, by default includes 4+ custom widget with header advertisement widget area, which custom widget you can easily add in left or right sidebar widget area or footer widget area. kathmag also supports many more 3rd party plugins, compatible with Jetpack, Contact Form 7, AccessPress Social Share, AccessPress, page builder, WooCommerce, Social Counter and many more. Official Support Forum: Support Full Demo: http://demo.sparklewpthemes.com/kathmag/demos/ and Docs: http://docs.sparklewpthemes.com/kathmag


== Translation ==

Kathmag theme is translation ready.


== Copyright ==

Kathmag WordPress Theme, Copyright (C) 2018 Sparkle Themes.
Kathmag is distributed under the terms of the GNU GPL


== Installation ==
    
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.


== Frequently Asked Questions ==

= Does this theme support any plugins? =

Theme supports WooCommerce and some other external plugins like element page builder, Page Builder by SiteOrigin, Jetpack, Contact Form 7 and many more plugins.

= Where can I find theme all features ? =

You can check our Theme features at http://sparklewpthemes.com/wordpress-themes/kathmag/

= Where can I find theme demo? =

You can check our Theme Demo at http://demo.sparklewpthemes.com/kathmag/demos/


== License ==

* Based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)

* Breadcrumbs Trail https://github.com/justintadlock/breadcrumb-trail, Copyright and License : Breadcrumb Trail is licensed under the GNU GPL, version 2 or later, 2008 – 2015 © Justin Tadlock.

* Bootstrap https://github.com/twbs/bootstrap/blob/master/LICENSE, Copyright (c) 2011-2016 Twitter, Inc., [MIT](http://opensource.org/licenses/MIT)

* Font Awesome http://fontawesome.io/license/, [SIL OFL 1.1, MIT](http://scripts.sil.org/OFL, http://opensource.org/licenses/mit-license.html)

* Animate https://github.com/daneden/animate.css, Copyright (c) 2018 Daniel Eden, [MIT](http://opensource.org/licenses/MIT)

* Owl Carousel https://github.com/OwlCarousel2/OwlCarousel2/, Copyright (c) 2014 Owl, [MIT](http://opensource.org/licenses/MIT)

* Stellarnav https://github.com/vinnymoreira/stellarnav, Copyright (c) 2016 Vinny Moreira, [MIT](http://opensource.org/licenses/MIT)

* Retina https://github.com/strues/retinajs, Copyright (c) 2013-2017 Steven Truesdell, John Newman, Ben Atkin, and other contributors, [MIT](http://opensource.org/licenses/MIT)

* Match Height https://github.com/liabru/jquery-match-height, Copyright (c) 2015 Liam Brummitt, [MIT](http://opensource.org/licenses/MIT)

* Theia Sticky Sidebar https://github.com/WeCodePixels/theia-sticky-sidebar, Copyright (c) 2014 Liviu Cristian Mirea Ghiban, [MIT](http://opensource.org/licenses/MIT)

* Sniper Front-End 3 https://github.com/h1dd3nsn1p3r/sniper-front-end, Copyright (c) 2017 Anuj Subedi, [MIT](http://opensource.org/licenses/MIT)

* Choosen https://github.com/harvesthq/chosen, Copyright (c) 2011-2018 Harvest, [MIT](http://opensource.org/licenses/MIT)

* Easy Ticker https://github.com/vaakash/jquery-easy-ticker, Copyright (c) 2014 Aakash Chakravarthy, [MIT](http://opensource.org/licenses/MIT)

* Smooth Scroll https://github.com/galambalazs/smoothscroll-for-websites, Copyright (c) 2010-2015 Balazs Galambosi, [MIT](http://opensource.org/licenses/MIT)


Images used in screenshot

https://pixabay.com/en/pink-hair-hairstyle-women-1450045/         License CC0 Public Domain
https://pixabay.com/en/woman-female-bike-beauty-model-695451/     License CC0 Public Domain
https://pixabay.com/en/tiger-cat-animal-predator-roar-3264048/    License CC0 Public Domain
https://pixabay.com/en/woman-portrait-face-model-canon-659352/    License CC0 Public Domain
https://www.pexels.com/photo/woman-hat-portrait-style-37649/      License CC0 Public Domain
https://pixabay.com/en/girl-woman-people-caucasian-women-1561989/ License CC0 Public Domain


Cusotm Other Images  
news-block-1.png self created GPLv2
news-block-2.png self created GPLv2
news-block-3.png self created GPLv2
news-block-6.png self created GPLv2
news-block-7.png self created GPLv2
news-block-8.png self created GPLv2
news-block-9.png self created GPLv2
news-block-11.png self created GPLv2
news-block-12.png self created GPLv2
news-block-14.png self created GPLv2
news-block-15.png self created GPLv2
news-block-16.png self created GPLv2
selected.png self created GPLv2
left-sidebar.png self created GPLv2
right-sidebar.png self created GPLv2
no-sidebar.png self created GPLv2


== Changelog ==

= 1.0.5 - 21st July 2021 =
* WordPress 5.8 Compatible

= 1.0.4 - 30th January 2020 =

** Add Primary and Secondary dynamic color options.
** Make theme fully compatible with RTL(Right to left) languages compatible.
** Make theme fully translation ready with compatible polylang & WPML plugins.
** .Pot file updates with recent change.
** Add documentation link in customizer.
** Make theme compatible with must popular elementor page builder plugins. 

= 1.0.3 - octo 9th, 2018 =

* Remove image below single post title.
* Chnage screenshot.

= 1.0.2 - May 7th, 2018 =

* Add one click demo import.
* Add in customizer to move PRO version button & link.
* Re-generate translate .pot file.


= 1.0.1 - April 29, 2018 =

* Unused codes and files are removed.
* Proper escaping done.
* Breadcrumb Trail has been updated.
* Uniform code used to acquire files.
* Theme description has been changed.
* Other minor fixes.

= 1.0.0 - April 15, 2018 =

* Initial release



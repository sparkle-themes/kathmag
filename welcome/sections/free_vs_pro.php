<div class="free-vs-pro-info-wrap">
    <div class="free-vs-pro-info">
        <h4><?php esc_html_e('ONE TIME PAYMENT', 'kathmag'); ?></h4>
        <p><?php esc_html_e('No renewal needed', 'kathmag'); ?></p>
    </div>

    <div class="free-vs-pro-info">
        <h4><?php esc_html_e('UNLIMITED DOMAIN LICENCE', 'kathmag'); ?></h4>
        <p><?php esc_html_e('Use in as many websites as you need', 'kathmag'); ?></p>
    </div>

    <div class="free-vs-pro-info">
        <h4><?php esc_html_e('FREE UPDATES FOR LIFETIME', 'kathmag'); ?></h4>
        <p><?php esc_html_e('Keep up to date', 'kathmag'); ?></p>
    </div>
</div>

<table class="comparison-table">
	<tr>
		<td>
			<span><?php esc_html_e('Upgrade to Pro', 'kathmag'); ?></span>
			<p><?php esc_html_e('Upgrade to pro version for additional features and better supports.', 'kathmag'); ?></p>
		</td>
		<td colspan="2">
			<a target="__blank" class="buy-pro-btn" href="https://sparklewpthemes.com/wordpress-themes/kathmagpro/"><?php esc_html_e('Buy Now ($55 only)', 'kathmag'); ?></a>
		</td>
	</tr>
	<tr>
		<th><?php esc_html_e('Features', 'kathmag'); ?></th>
		<th><?php esc_html_e('Free', 'kathmag'); ?></th>
		<th><?php esc_html_e('Pro', 'kathmag'); ?></th>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('One Click Demo Import', 'kathmag'); ?></span>
			<p><?php esc_html_e('Just simple with once click you can import any pages with sample content form the demo easily.', 'kathmag'); ?>
		</td>
		<td><?php esc_html_e('1 Demo Only', 'kathmag'); ?></td>
		<td><?php esc_html_e('5 Demos', 'kathmag'); ?></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('Multiple Header Layouts and Settings', 'kathmag'); ?></span>
			<p><?php esc_html_e('The premium version gives the option to choose from 4 different header layout per as you want.', 'kathmag'); ?></p>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/no.png'); ?>" alt="No"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('Unlimited Color Options', 'kathmag'); ?></span>
			<p><?php esc_html_e('The free version has a basic color option but the premium version has advanced color options that allow customizing the color theme primary colors.', 'kathmag'); ?>
		</td>
		<td><?php esc_html_e('Basic', 'kathmag'); ?></td>
		<td><?php esc_html_e('Advanced', 'kathmag'); ?></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('Remove Footer Credit Text', 'kathmag'); ?></span>
			<p><?php esc_html_e('The premium version easily allows to remove or change the footer credit text.', 'kathmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/no.png'); ?>" alt="No"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('Advanced Top Header Settings', 'kathmag'); ?></span>
			<p><?php esc_html_e('Kathmag comes with an option for the top bar where you can add contact address, social icon, nav menu.', 'kathmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/no.png'); ?>" alt="No"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('Custom Inbuilt Widgets', 'kathmag'); ?></span>
			<p><?php esc_html_e('With custom widget and Site origin page builder, you can play to create various website layout.', 'kathmag'); ?>
		</td>
		<td>4+</td>
		<td>7+</td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('Single Post Vote Up & Vote Down', 'kathmag'); ?></span>
			<p><?php esc_html_e('Kathmag Pro theme have easily to like / Dislike OR vote Up / Vote Down for single post, theme have also post visiter auto count.', 'kathmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/no.png'); ?>" alt="No"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('Category Post Layouts', 'kathmag'); ?></span>
			<p><?php esc_html_e('Kathmag Pro display 6 different categories layout options, select any one layout & keep your visitors engaged on your website.', 'kathmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/no.png'); ?>" alt="No"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('Unique Post Format Options', 'kathmag'); ?></span>
			<p><?php esc_html_e('Single Posts Format options helps you to show single posts different style with video, gallery, audio and (Standard,Classic, full Screen) post.', 'kathmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/no.png'); ?>" alt="No"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('PreLoader Option', 'kathmag'); ?></span>
			<p><?php esc_html_e('The loading screen that appears until the website is fully loaded. The premium version has the option to choose from 10 preloaders per as you want.', 'kathmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/no.png'); ?>" alt="No"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('Sidebar Layout Options', 'kathmag'); ?></span>
			<p><?php esc_html_e('The premium version has the option to change the sidebar layout universally or change the sidebar for individual post/page. Moreover, it allows choosing the sidebar widgets for individual post/page.', 'kathmag'); ?>
		</td>
		<td><?php esc_html_e('Basic', 'kathmag'); ?></td>
		<td><?php esc_html_e('Advanced', 'kathmag'); ?></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('Website Layout (Fullwidth or Boxed)', 'kathmag'); ?></span>
			<p><?php esc_html_e('The premium version has the option to change the website layout to be full width or boxed. Additionally, you can set the width of the website container.', 'kathmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('WooCommerce Compatible', 'kathmag'); ?></span>
			<p><?php esc_html_e('The premium Version has enhanced WooCommerce options like adding cart icon in the menu, product page settings, sidebar layout settings and more.', 'kathmag'); ?>
		</td>
		<td><?php esc_html_e('Basic', 'kathmag'); ?></td>
		<td><?php esc_html_e('Advanced', 'kathmag'); ?></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('Translation Ready', 'kathmag'); ?></span>
			<p><?php esc_html_e('Both free and pro version are fully translation ready.', 'kathmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>
	<tr>
		<td>
		<span><?php esc_html_e('Search Engine Optimization', 'kathmag'); ?></span>
		<p><?php esc_html_e('Follows best SEO practices so that your website always rank higher in Search Engines.', 'kathmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>

	<tr>
		<td>
			<span><?php esc_html_e('Major Browser Compatible', 'kathmag'); ?></span>
			<p><?php esc_html_e('The website shows good and runs smoothly in all major browsers.', 'kathmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('Responsive - Mobile Friendly', 'kathmag'); ?></span>
			<p><?php esc_html_e('Adapts to any screen size and display beautifully.', 'kathmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('Child Theme Support', 'kathmag'); ?></span>
			<p><?php esc_html_e('Child theme support makes it easy for you to transfer your settings and options when you update and change you themes.', 'kathmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('SEO Friendly', 'kathmag'); ?></span>
			<p><?php esc_html_e('SEO refers to search engine optimization, or the process of optimizing a website so that people can easily find it via search engines like Google.', 'kathmag'); ?>
		</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('Fast and Friendly Support', 'kathmag'); ?></span>
			<p><?php esc_html_e('For Premium theme, the user will get a reply in 10 hours or less.', 'kathmag'); ?>
		</td>
		<td>-</td>
		<td><img src="<?php echo esc_url(get_template_directory_uri().'/welcome/css/yes.png'); ?>" alt="Yes"></td>
	</tr>
	<tr>
		<td>
			<span><?php esc_html_e('Upgrade to Pro', 'kathmag'); ?></span>
			<p><?php esc_html_e('Upgrade to pro version for additional features and better supports.', 'kathmag'); ?></p>
		</td>
		<td colspan="2">
			<a target="__blank" class="buy-pro-btn" href="https://sparklewpthemes.com/wordpress-themes/kathmagpro/"><?php esc_html_e('Buy Now ($55 only)', 'kathmag'); ?></a>
		</td>
	</tr>
</table>

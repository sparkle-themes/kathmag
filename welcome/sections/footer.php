<div class="welcome-upgrade-wrap">
    <div class="welcome-upgrade-header">
        <h3><?php printf(esc_html__('Premium Version of %s', 'kathmag'), $this->theme_name); ?></h3>
        <p><?php echo sprintf(esc_html__('Check out the demos that you can create with the premium version of the %s theme. 5+ Pre-defined demos can be imported with just one click in the premium version.', 'kathmag'), $this->theme_name); ?></p>
    </div>

    <div class="recomended-plugin-wrap">
        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://sparklewpthemes.com/wp-content/uploads/2018/05/Kathmag-Pro-Magazine.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('kathmag Pro - Main','kathmag'); ?></span>
                <span class="plugin-btn-wrapper">
					<a target="_blank" href="https://demo.sparklewpthemes.com/kathmagpro/" class="button button-primary"><?php echo esc_html__('Preview', 'kathmag'); ?></a>
				</span>
            </div>
        </div>

        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://sparklewpthemes.com/wp-content/uploads/2018/05/Kathmag-Pro-Travel.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('kathmag Pro - Travel','kathmag'); ?></span>
				<span class="plugin-btn-wrapper">
					<a target="_blank" href="https://demo.sparklewpthemes.com/kathmagpro/travel/" class="button button-primary"><?php echo esc_html__('Preview', 'kathmag'); ?></a>
				</span>
            </div>
        </div>
        
        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://sparklewpthemes.com/wp-content/uploads/2018/05/Kathmag-Pro-Sport.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('kathmag Pro - Sport','kathmag'); ?></span>
				<span class="plugin-btn-wrapper">
					<a target="_blank" href="https://demo.sparklewpthemes.com/kathmagpro/sport/" class="button button-primary"><?php echo esc_html__('Preview', 'kathmag'); ?></a>
				</span>
            </div>
        </div>

        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://sparklewpthemes.com/wp-content/uploads/2018/05/Kathmag-Pro-Technology.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('kathmag Pro - Technology','kathmag'); ?></span>
				<span class="plugin-btn-wrapper">
					<a target="_blank" href="https://demo.sparklewpthemes.com/kathmagpro/tech/" class="button button-primary"><?php echo esc_html__('Preview', 'kathmag'); ?></a>
				</span>
            </div>
        </div>

        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://sparklewpthemes.com/wp-content/uploads/2018/05/Kathmag-Pro-Glmour.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('kathmag Pro - Glamour','kathmag'); ?></span>
                <span class="plugin-btn-wrapper">
                    <a target="_blank" href="https://demo.sparklewpthemes.com/kathmagpro/glamour/" class="button button-primary"><?php echo esc_html__('Preview', 'kathmag'); ?></a>
                </span>
            </div>
        </div>
        
    </div>
</div>

<div class="welcome-upgrade-box">
    <div class="upgrade-box-text">
        <h3><?php echo esc_html__('Upgrade To Premium Version (14 Days Money Back Guarantee)', 'kathmag'); ?></h3>
        <p><?php echo sprintf(esc_html__('With %s Pro theme you can create a beautiful website. If you want to unlock more possibilities then upgrade to the premium version, try the Premium version and check if it fits your need or not. If not, we have 14 days money-back guarantee.', 'kathmag'), $this->theme_name); ?></p>
    </div>

    <a class="upgrade-button" href="https://sparklewpthemes.com/wordpress-themes/kathmagpro/" target="_blank"><?php esc_html_e('Upgrade Now', 'kathmag'); ?></a>
</div>

